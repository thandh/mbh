﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Include these namespaces to use BinaryFormatter
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Beetsoft.Browser.Scripts {
	// Demo class to illustrate the usage of the FileBrowser script
	// Able to save and load files containing serialized data (e.g. text)
	public class BrowserCaller : MonoBehaviour
    {
        // Use to determine is open or not
        [SerializeField] private bool isOpen = false;		
        
        // Use the file browser prefab
		public GameObject FileBrowserPrefab;

		// Define a file extension
		[SerializeField] private string fileExtension;

		// Input field to get text to save
		private GameObject _textToSaveInputField;

		// Label to display loaded text
		private GameObject _loadedText;

		// Text to save intermediate input result
		private string _textToSave = null;

        // PNG to save intermediate input result
        private Texture2D _PNGToSave = null;

        public bool PortraitMode;

		// Find the input field, label objects and add a onValueChanged listener to the input field
		private void Start()
        {	
			
		}

		// Updates the file extension
		public void UpdateExtension(string text)
        {
			fileExtension = text.ToLower();
		}

        // Updates the text to save with the new input (current text in input field)
        public void UpdateTextToSave(string text)
        {
            _textToSave = text;
        }

        // Updates the PNG to save with the new input (current text in input field)
        public void UpdatePNGtoSave(Texture2D Texture2D)
        {
            _PNGToSave = Texture2D;
        }

        // Open the file browser using boolean parameter so it can be called in GUI
        public void OpenFileBrowser(bool saving)
        {
            isOpen = true;
			OpenFileBrowser(saving ? FileBrowserMode.Save : FileBrowserMode.Load);
		}        

        // Open a file browser to save and load files
        private void OpenFileBrowser(FileBrowserMode fileBrowserMode) {
			// Create the file browser and name it
			GameObject fileBrowserObject = Instantiate(FileBrowserPrefab, transform);
			fileBrowserObject.name = "FileBrowser";
			// Set the mode to save or load
			FileBrowser fileBrowserScript = fileBrowserObject.GetComponent<FileBrowser>();
			fileBrowserScript.SetupFileBrowser(PortraitMode ? ViewMode.Portrait : ViewMode.Landscape);
			if (fileBrowserMode == FileBrowserMode.Save) {
				fileBrowserScript.SaveFilePanel(this, "SaveFileUsingPath", "DemoText", fileExtension);
			} else {
				fileBrowserScript.OpenFilePanel(this, "LoadFileUsingPath", fileExtension);
			}
		}

		// Saves a file with the textToSave using a path
		private void SaveFileUsingPath(string path) {

            if (string.IsNullOrEmpty(path))
            { Debug.Log("Invalid path!!"); return; }

            switch (fileExtension)
            {
                case "txt":
                    if (string.IsNullOrEmpty(_textToSave))
                    {
                        Debug.Log("Empty file given");
                        return;
                    }

                    BinaryFormatter bFormatter = new BinaryFormatter();
                    // Create a file using the path
                    FileStream file = File.Create(path);
                    // Serialize the data (textToSave)
                    bFormatter.Serialize(file, _textToSave);
                    // Close the created file
                    file.Close();

                    break;

                case "png":
                    if (_PNGToSave == null)
                    {
                        Debug.Log("Empty png given");
                        return;
                    }

                    byte[] bytes = _PNGToSave.EncodeToPNG();
                    File.WriteAllBytes(path, bytes);
                    break;
            }

            isOpen = false;  
		}

		// Loads a file using a path
		private void LoadFileUsingPath(string path) {
			if (path.Length != 0) {
				BinaryFormatter bFormatter = new BinaryFormatter();
				// Open the file using the path
				FileStream file = File.OpenRead(path);
				// Convert the file from a byte array into a string
				string fileData = bFormatter.Deserialize(file) as string;
				// We're done working with the file so we can close it
				file.Close();
				// Set the LoadedText with the value of the file
				_loadedText.GetComponent<Text>().text = "Loaded data: \n" + fileData;
			} else {
				Debug.Log("Invalid path given");
			}
		}
	}
}
﻿using UnityEngine;
using TouchScript.Gestures.TransformGestures;

namespace Beetsoft.Layout.CameraControl
{    
    public enum eCheckCamMoveState
    {
        On,
        Off        
    }

    public class CameraWorld : MonoBehaviour
    {
        private static CameraWorld _Instance = null;
        public static CameraWorld Instance { get { return _Instance; } }
        
        private CameraWorld cameraWorld { get { return this; } }

        private CameraGrid _cameraGrid = null;
        private CameraGrid cameraGrid { get { if (_cameraGrid == null) _cameraGrid = FindObjectOfType<CameraGrid>(); return _cameraGrid; } }

        private CameraUI _cameraUI = null;
        private CameraUI cameraUI { get { if (_cameraUI == null) _cameraUI = FindObjectOfType<CameraUI>(); return _cameraUI; } }

        public bool isAllow = true;        
        public eCheckCamMoveState eCheckCamMoveState = eCheckCamMoveState.On;
        public bool isHitUI = false;

        public ScreenTransformGesture OneFingerGesture;
        public ScreenTransformGesture TwoFingerGesture;
        public float PanSpeed = 200f;        
        public float ZoomSpeed = 10f;

        private void Awake()
        {
            if (_Instance == null)
                _Instance = this;
            else Destroy(gameObject);             
        }

        private void OnEnable()
        {
            if (Instance == null) return;
                     
            OneFingerGesture.Transformed += oneFingerTransformedHandler;
            TwoFingerGesture.Transformed += twoFingerTransformHandler;            
        }

        private void OnDisable()
        {
            if (Instance == null) return;

            OneFingerGesture.Transformed -= oneFingerTransformedHandler;
            TwoFingerGesture.Transformed -= twoFingerTransformHandler;            
        }

        private void oneFingerTransformedHandler(object sender, System.EventArgs e)
        {
            if (!isAllow) return;
            if (eCheckCamMoveState != eCheckCamMoveState.On) return;
            if (isHitUI) return;

            transform.position += (OneFingerGesture.DeltaPosition) * PanSpeed;            
        }

        private void twoFingerTransformHandler(object sender, System.EventArgs e)
        {
            if (!isAllow) return;
            if (eCheckCamMoveState != eCheckCamMoveState.On) return;
            if (isHitUI) return;

            cameraWorld.GetComponent<Camera>().orthographicSize += (TwoFingerGesture.DeltaScale - 1) * ZoomSpeed;
            cameraGrid.GetComponent<Camera>().orthographicSize += (TwoFingerGesture.DeltaScale - 1) * ZoomSpeed;
        }

        private void Update()
        {
            if (!Input.GetMouseButton(0) && Input.touchCount <= 0) return;            

            Vector3 screenpos = Vector3.zero;
            if (Input.GetMouseButton(0)) screenpos = Input.mousePosition;
            else if (Input.touchCount > 0) screenpos = Input.touches[0].position;
                        
            Ray ray = cameraUI.GetComponent<Camera>().ScreenPointToRay(screenpos + cameraUI.transform.position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000f))
            {
                Debug.Log(hit.transform.gameObject.name);
                Debug.DrawRay(ray.origin, ray.direction * 1000f, Color.blue);
                CheckSeteCheckCamMoveState(hit.transform.gameObject.tag);
                return;
            }

            ray = cameraWorld.GetComponent<Camera>().ScreenPointToRay(screenpos);            
            if (Physics.Raycast(ray, out hit, 1000f))
            {
                Debug.Log(hit.transform.gameObject.name);
                Debug.DrawRay(ray.origin, ray.direction * 1000f, Color.yellow);
                CheckSeteCheckCamMoveState(hit.transform.gameObject.tag);
                return;
            }
        }

        void CheckSeteCheckCamMoveState(string tag)
        {
            switch (tag)
            {
                case "eCheckCamMoveState.On":
                    if (eCheckCamMoveState != eCheckCamMoveState.On) eCheckCamMoveState = eCheckCamMoveState.On;
                    break;
                case "eCheckCamMoveState.Off":
                    if (eCheckCamMoveState != eCheckCamMoveState.Off) eCheckCamMoveState = eCheckCamMoveState.Off;
                    break;
            }
        }
    }
}
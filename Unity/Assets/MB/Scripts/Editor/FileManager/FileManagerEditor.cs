﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Beetsoft.World.Grid;
using Beetsoft.JsonData;

[CustomEditor(typeof(FileManager))]
public class FileManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        FileManager FileManager = (FileManager)target;

        if (GUILayout.Button("Output Txt"))
        {
            FileManager.OutputTxt();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Beetsoft.World.Grid;

[CustomEditor(typeof(FloorButton))]
public class FloorButtonEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        FloorButton FloorButton = (FloorButton)target;

        if (GUILayout.Button("Load Grids"))
        {
            FloorButton.LoadImage();
        }
    }
}

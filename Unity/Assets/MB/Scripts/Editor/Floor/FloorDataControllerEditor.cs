﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Beetsoft.World.Grid;

[CustomEditor(typeof(FloorDataController))]
public class FloorDataControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        FloorDataController FloorDataController = (FloorDataController)target;

        if (GUILayout.Button("Load Sprites"))
        {
            FloorDataController.LoadSprites();
        }
    }
}

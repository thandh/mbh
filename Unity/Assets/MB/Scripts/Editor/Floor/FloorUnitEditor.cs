﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Beetsoft.World.Floors;

[CustomEditor(typeof(FloorUnit))]
public class FloorUnitEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        FloorUnit FloorUnit = (FloorUnit)target;

        if (GUILayout.Button("Load Sprites Editor"))
        {
            FloorUnit.LoadSprite();
        }

        if (GUILayout.Button("Self Destroy"))
        {
            FloorUnit.SelfDestroy();
        }

        if (GUILayout.Button("Set IsPlaced"))
        {
            FloorUnit.SetIsPlace();
        }
    }
}

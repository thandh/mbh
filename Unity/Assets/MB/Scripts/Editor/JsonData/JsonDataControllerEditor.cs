﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Beetsoft.World.Grid;
using Beetsoft.JsonData;

[CustomEditor(typeof(JsonDataController))]
public class JsonDataControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        JsonDataController JsonDataController = (JsonDataController)target;

        if (GUILayout.Button("Printttt Result"))
        {
            JsonDataController.PrintResult();
        }        
    }
}

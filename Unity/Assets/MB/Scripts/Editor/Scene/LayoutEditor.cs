﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using SceneControlling;

[CustomEditor(typeof(LayoutSC))]
public class LayoutSCEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        LayoutSC LayoutSC = (LayoutSC)target;

        if (GUILayout.Button("Load Test"))
        {
            LayoutSC.LoadASprite(null);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UICamCapture))]
public class UICamCaptureEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        UICamCapture UICamCapture = (UICamCapture)target;

        if (GUILayout.Button("Open In Editor"))
        {
            UICamCapture.OpenInEditor();
        }

        if (GUILayout.Button("Close In Editor"))
        {
            UICamCapture.CloseInEditor();
        }
    }
}

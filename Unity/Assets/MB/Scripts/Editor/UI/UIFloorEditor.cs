﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UIFloor))]
public class UIFloorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        UIFloor UIFloor = (UIFloor)target;

        if (GUILayout.Button("Load Sprites Editor"))
        {
            UIFloor.LoadSprites();
        }
    }
}

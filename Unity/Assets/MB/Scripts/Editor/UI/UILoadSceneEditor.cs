﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UILoadScene))]
public class UILoadSceneEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        UILoadScene UILoadScene = (UILoadScene)target;

        if (GUILayout.Button("Alpha Out"))
        {
            UILoadScene.AlphaOutEditor();
        }

        if (GUILayout.Button("Alpha In"))
        {
            UILoadScene.AlphaInEditor();
        }
    }
}

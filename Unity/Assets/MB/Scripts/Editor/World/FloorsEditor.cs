﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Beetsoft.World.Grid;
using Beetsoft.World.Floors;

[CustomEditor(typeof(Floors))]
public class FloorsEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Floors Floors = (Floors)target;

        if (GUILayout.Button("Load Floors"))
        {
            Floors.LoadFloorUnitList();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Beetsoft.World.Grid;

[CustomEditor(typeof(Grids))]
public class GridsEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Grids Grids = (Grids)target;

        if (GUILayout.Button("Load Grids"))
        {
            Grids.LoadGridsList();
        }

        if (GUILayout.Button("Delete Grids"))
        {
            Grids.DeleteGrids();
        }

        if (GUILayout.Button("Regenerate Grids"))
        {
            Grids.RegenerateGridsEditor();
        }

        if (GUILayout.Button("Update Size"))
        {
            Grids.UpdateSizes();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using SceneControlling;
using Beetsoft.World.Layouts.Line;

[CustomEditor(typeof(Line))]
public class LineEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Line Line = (Line)target;

        if (GUILayout.Button("Self Destroy"))
        {
            Line.SelfDestroy();
        }
    }
}

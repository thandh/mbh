﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Beetsoft.World.Layouts.Line;

[CustomEditor(typeof(Lines))]
public class LinesEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Lines Lines = (Lines)target;

        if (GUILayout.Button("Load Lines"))
        {
            Lines.LoadLineList();
        }

        if (GUILayout.Button("Delete Lines"))
        {
            Lines.DeleteLines();
        }
    }
}

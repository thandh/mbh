﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using SceneControlling;
using Beetsoft.World.Layouts.Point;

[CustomEditor(typeof(Point))]
public class PointEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Point Point = (Point)target;

        if (GUILayout.Button("Self Destroy"))
        {
            Point.SelfDestroy();
        }

        if (GUILayout.Button("Select Once"))
        {
            Point.SelectOnce();
        }        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Beetsoft.World.Layouts.Point;

[CustomEditor(typeof(Points))]
public class PointsEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Points Points = (Points)target;

        if (GUILayout.Button("Load Points"))
        {
            Points.LoadPointList();
        }

        if (GUILayout.Button("Create Point"))
        {
            Points.CreatePoint();
        }

        if (GUILayout.Button("Delete Points"))
        {
            Points.DeletePoints();
        }

        if (GUILayout.Button("Finish Drawing"))
        {
            Points.ConnectLastAndStartPoint();
        }

        if (GUILayout.Button("Force Connect Interior"))
        {
            Points.ForceConnectInterior();
        }
    }
}

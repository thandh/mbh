﻿using LitJson;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FileManager : MonoBehaviour
{

    public void OutputTxt()
    {
        string path1 = Application.dataPath + "/layout1.txt";
        string path2 = Application.persistentDataPath + "/layout2.txt";

        StreamWriter strw1 = new StreamWriter(path1);
        //strw1.Write(JsonMapper.ToJson(datatest).ToString());
        strw1.Close();

        StreamWriter strw2 = new StreamWriter(path2);
        //strw2.Write(JsonMapper.ToJson(datatest).ToString());
        strw2.Close();
    }
}

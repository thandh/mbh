﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beetsoft.JsonData
{
    public class HouseData
    {
        public int FloorNumber;
        public int FloorNumber1;
        public double DirectionX;
        public double DirectionY;

        public LayoutData LayoutData;

        public HouseData()
        {
            this.FloorNumber = 0;
            this.FloorNumber1 = 0;
            this.DirectionX = 0;
            this.DirectionY = 0;
        }

        public HouseData(int FloorNumber, double DirectionX, double DirectionY, LayoutData LayoutData)
        {
            this.FloorNumber = FloorNumber;
            this.FloorNumber1 = FloorNumber;
            this.DirectionX = DirectionX;
            this.DirectionY = DirectionY;
            this.LayoutData = LayoutData;
        }
    }        
}
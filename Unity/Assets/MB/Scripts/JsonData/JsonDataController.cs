﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;

namespace Beetsoft.JsonData
{
    public class JsonDataController : MonoBehaviour
    {
        HouseData datatest = new HouseData();

        [HideInInspector] public string strtest = "";

        private void Start()
        {
            datatest = new HouseData(1, 2d, 3d, new JsonData.LayoutData("area1"));

            Debug.Log(JsonMapper.ToJson(datatest).ToString());
        }

        public void PrintResult()
        {
            datatest = JsonMapper.ToObject<HouseData>(strtest);
            Debug.Log(datatest.LayoutData.AreaType);
        }

        
    }
}
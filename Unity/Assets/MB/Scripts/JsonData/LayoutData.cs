﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beetsoft.JsonData
{
    public class LayoutData
    {
        public string AreaType;

        public LayoutData()
        {
            this.AreaType = "";            
        }

        public LayoutData(string AreaType)
        {
            this.AreaType = AreaType;
        }
    }
}
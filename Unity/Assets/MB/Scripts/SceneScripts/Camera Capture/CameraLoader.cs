﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLoader : MonoBehaviour
{    
    void Start()
    {
        WebCamTexture camera = new WebCamTexture();
        Renderer renderer = GetComponent<Renderer>();

        // set the size of the plane
        var height = Camera.main.orthographicSize * 2;
        var width = height * (Screen.width / Screen.height);
        transform.localScale = new Vector3(width, height, 1);

        renderer.material.mainTexture = camera;
        camera.Play();
    }    
}
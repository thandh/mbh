﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Beetsoft.UI.Scripts;

namespace SceneControlling
{
    public class CameraCaptureSC : SceneControl
    {
        // Use this for initialization
        public override void Start ()
        {
            StartCoroutine(DoStart());
        }
        
        IEnumerator DoStart()
        {
            yield return new WaitUntil(() => UICamCapture.Instance != null);
            UICamCapture.Instance.Open();
            yield return new WaitUntil(() => UICamCapture.Instance.uiState == UIState.Open);

            yield return new WaitUntil(() => UIControl.Instance != null);
            UIControl.Instance.Open();
            yield return new WaitUntil(() => UIControl.Instance.uiState == UIState.Open);

            yield return new WaitUntil(() => UILoadScene.Instance.IsAlphaIn());

            base.Start();
        }

        public override void Unload()
        {
            UICamCapture.Instance.SeftDestroy();
            UIControl.Instance.SeftDestroy();

            base.Unload();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorDataController : MonoBehaviour
{
    [SerializeField] private string path = "Floor/";
    public Sprite[] Sprites = new Sprite[0];
    
    public void LoadSprites()
    {
        Sprites = Resources.LoadAll<Sprite>(path);
    }
}

﻿using Beetsoft.Layout.CameraControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SceneControlling
{
    public class FloorSC : SceneControl
    {
        FloorDataController _FloorDataController = null;
        public FloorDataController FloorDataController { get { if (_FloorDataController == null) _FloorDataController = GetComponent<FloorDataController>(); return _FloorDataController; } }

        CameraWorld _CameraController = null;
        CameraWorld CameraController { get { if (_CameraController == null) _CameraController = FindObjectOfType<CameraWorld>(); return _CameraController; } }

        CameraGrid _CameraGrid = null;
        CameraGrid CameraGrid { get { if (_CameraGrid == null) _CameraGrid = FindObjectOfType<CameraGrid>(); return _CameraGrid; } }

        CameraUI _CameraUI = null;
        CameraUI CameraUI { get { if (_CameraUI == null) _CameraUI = FindObjectOfType<CameraUI>(); return _CameraUI; } }

        void Update()
        {
            
        }
    }
}
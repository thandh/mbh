﻿using Beetsoft.UI.Scripts;
using Beetsoft.World.Layouts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SceneControlling
{
    public class LayoutSC : SceneControl
    {
        private WorldLayout _WorldLayout = null;
        public WorldLayout WorldLayout { get { if (_WorldLayout == null) _WorldLayout = FindObjectOfType<WorldLayout>(); return _WorldLayout; } }
        
        [SerializeField] private Sprite SpriteTest = null;

        public override void Start ()
        {
            StartCoroutine(DoStart());
        }

        IEnumerator DoStart()
        {
            yield return new WaitUntil(() => UILayout.Instance != null);
            UILayout.Instance.Open();
            yield return new WaitUntil(() => UILayout.Instance.uiState == UIState.Open);

            yield return new WaitUntil(() => UILayoutControl.Instance != null);
            UILayoutControl.Instance.Open();
            yield return new WaitUntil(() => UILayoutControl.Instance.uiState == UIState.Open);

            yield return new WaitUntil(() => UILoadScene.Instance.IsAlphaIn());

            base.Start();
        }

        public override void Unload()
        {
            UILayout.Instance.SeftDestroy();
            UILayoutControl.Instance.SeftDestroy();

            base.Unload();
        }

        public void LoadASprite(Sprite Sprite = null)
        {
            WorldLayout.LoadASprite(Sprite == null ? SpriteTest : Sprite);
        }
}
}
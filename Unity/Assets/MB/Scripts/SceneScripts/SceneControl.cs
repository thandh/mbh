﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SceneControlling
{
    public enum eSceneState
    {
        Nothing,
        Loaded,
        Unloaded
    }

    public class SceneControl : MonoBehaviour
    {
        public eSceneState SceneState = eSceneState.Nothing;

        public virtual void Start()
        {
            SceneState = eSceneState.Loaded;
        }

        public virtual void Load()
        {
            SceneState = eSceneState.Loaded;
        }

        public virtual void Unload()
        {
            SceneState = eSceneState.Unloaded;
        }

        public void ForceUnload()
        {
            
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasUI : MonoBehaviour, iCanvasUI
{
    public Transform HolderDestroy;
    public Transform HolderCheckDup;

    public virtual void ToDestroy()
    {
        
    }

    public virtual void DeleteAllChildren(Transform tr)
    {
        StartCoroutine(C_DeleteAllChildren(tr));
    }

    IEnumerator C_DeleteAllChildren(Transform tr)
    {
        int childCount = tr.childCount;
        for (int i = 0; i < childCount; i++)
        {
            Destroy(tr.GetChild(tr.childCount - 1).gameObject);
            yield return new WaitUntil(() => tr.childCount == childCount - i - 1);
        }
    }

    public virtual void ResetALayer(RectTransform rect)
    {
        Debug.Log("Reseting Layer " + rect.name);

        rect.localPosition = Vector3.zero;
        rect.localRotation = Quaternion.identity;
        rect.localScale = new Vector3(1f, 1f, 1f);
        rect.sizeDelta = new Vector2(0, 0);
    }

    public virtual void ResetAUIBase(RectTransform rect)
    {
        Debug.Log("Reseting uiBase " + rect.name);
        ResetALayer(rect);        
    }

    public virtual void ReArrageLayers()
    {
        StopCoroutine(C_ReArrangeLayers());
        StartCoroutine(C_ReArrangeLayers());
    }

    private IEnumerator C_ReArrangeLayers()
    {
        List<Transform> trans = new List<Transform>();

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name.Contains("Layer")) trans.Add(transform.GetChild(i));
        }

        for (int i = 0; i < trans.Count - 1; i++)
        {
            for (int j = 1; j < trans.Count; j++)
            {
                if (GetLayerIndex(trans[i].name) > GetLayerIndex(trans[j].name))
                {
                    int indexI = transform.GetChild(i).GetSiblingIndex();
                    int indexJ = transform.GetChild(j).GetSiblingIndex();
                    trans[i].transform.SetSiblingIndex(indexJ);
                    trans[j].transform.SetSiblingIndex(indexI);
                    Transform temp = trans[i]; trans[i] = trans[j]; trans[j] = temp;
                }
            }
        }

        yield break;
    }

    public virtual int GetLayerIndex(string Layer)
    {
        string l = Layer.Remove(0, 5);
        return int.Parse(l);
    }
}

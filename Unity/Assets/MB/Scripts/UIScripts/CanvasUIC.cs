﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class CanvasUIC : CanvasUI
{
    public static CanvasUIC Instance = null;    
    
    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        } else
        {
            ToDestroy();            
        }
    }    

    public override void ToDestroy()
    {
        StartCoroutine(C_ToDestroy());
    }

    IEnumerator C_ToDestroy()
    {
        ///Delete what must be deleted
        if (HolderDestroy)
        {
            DeleteAllChildren(HolderDestroy);
            yield return new WaitUntil(() => HolderDestroy.childCount == 0);
        }

        ///Check duplicate: Make sure use only one
        if (HolderCheckDup)
        {
            int index = 0;
            while (index < HolderCheckDup.childCount)
            {
                Transform tr = Instance.HolderCheckDup.Find(HolderCheckDup.GetChild(index).name);
                if (tr != null)
                {
                    Destroy(HolderCheckDup.GetChild(index).gameObject);
                    yield return new WaitUntil(() => HolderCheckDup.childCount == 0 || HolderCheckDup.GetChild(0).name != tr.name);
                } else
                {
                    index++;
                }
            }
        }

        ///Transfer UILayers->UIBases to CanvasUI
        List<RectTransform> Layers = new List<RectTransform>();
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform tran = transform.GetChild(0);
            if (!tran.name.Contains("Layer")) continue;
            Layers.Add(tran.GetComponent<RectTransform>());
        }

        for (int i = 0; i < Layers.Count; i++)
        {
            RectTransform layerRect = Layers[i];

            RectTransform IlayerRect = null;

            Transform IlayerTrans = Instance.transform.Find(Layers[i].name);            

            if (IlayerTrans != null)
                IlayerRect = IlayerTrans.GetComponent<RectTransform>();

            if (IlayerRect == null)
            {
                int IchildCount = Instance.transform.childCount;
                layerRect.SetParent(Instance.transform);
                yield return new WaitUntil(() => Instance.transform.childCount == IchildCount + 1);
                ResetALayer(layerRect);
            } else
            {
                for (int j = 0; j < layerRect.childCount; j++)
                {
                    int ILayerChildCount = IlayerRect.childCount;
                    RectTransform uiBaseRect = layerRect.GetChild(j).GetComponent<RectTransform>();
                    uiBaseRect.SetParent(IlayerRect);
                    yield return new WaitUntil(() => IlayerRect.childCount == ILayerChildCount + 1);
                    ResetAUIBase(uiBaseRect);
                }
            }
        }

        Destroy(gameObject);
    }    

    public CanvasUIC GetInstance()
    {
        return Instance;
    }    

    public override void ResetAUIBase(RectTransform rect)
    {
        base.ResetAUIBase(rect);
        rect.GetComponent<LayerUIC>().DoLayer();
    }    
}

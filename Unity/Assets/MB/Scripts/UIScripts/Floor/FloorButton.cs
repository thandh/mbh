﻿using Beetsoft.World.Floors;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FloorButton : MonoBehaviour, IPointerDownHandler
{
    Image _Image = null;
    Image Image { get { if (_Image == null) _Image = GetComponent<Image>(); return _Image; } }

    [SerializeField] Sprite _Sprite = null;

    Transform _Parent = null;
    Transform Parent { get { if (_Parent == null) _Parent = FindObjectOfType<UIFloor>().floorButtonHolder; return _Parent; } }

    Floors _Floors = null;
    Floors Floors { get { if (_Floors == null) _Floors = FindObjectOfType<Floors>(); return _Floors; } }

    public void LoadImage()
    {
        if (_Sprite == null) return;
        LoadImage(_Sprite);
    }

    public void LoadImage(Sprite Sprite)
    {
        SetImage(Sprite);
        Image.sprite = Sprite;
        Image.SetNativeSize();

        gameObject.name = Sprite.name;
    }

    public void SetImage(Sprite Sprite)
    {
        _Sprite = Sprite;
    }

    public void ResetTrans()
    {
        if (Parent == null) return;

        transform.SetParent(Parent);

        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = new Vector3(1, 1, 1);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (Floors.AFloorUnit != null) return;

        Floors.CreateAFloorUnit(eventData.position);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerUI : MonoBehaviour
{
    public string Layer = "";
    public int ChildIndex = -1;

    [HideInInspector] public Transform CanvasTransform = null;

    public virtual void Awake()
    {
        CanvasTransform = null;
    }

    public void SetLayer(string Layer)
    {
        this.Layer = Layer;
    }

    public void DoLayer()
    {
        if (string.IsNullOrEmpty(Layer)) return;

        if (CanvasTransform == null) return;

        Transform parent = CanvasTransform.Find(this.Layer);

        if (parent == null) parent = CreateALayer(Layer);

        transform.SetParent(parent);
        ResetARect(transform.GetComponent<RectTransform>());

        CanvasTransform.GetComponent<iCanvasUI>().ReArrageLayers();
    }

    public Transform CreateALayer(string Layer)
    {
        GameObject go = new GameObject();
        go.AddComponent<RectTransform>();
        go.name = Layer;
        go.transform.SetParent(CanvasTransform);
        ResetARectLayer(go.GetComponent<RectTransform>());
        return go.transform;
    }

    void ResetARectLayer(RectTransform rect)
    {
        ResetARect(rect);
        rect.anchorMin = Vector2.zero;
        rect.anchorMax = new Vector2(1f, 1f);
        rect.sizeDelta = new Vector2(0, 0);
    }

    void ResetARect(RectTransform rect)
    {
        rect.localPosition = Vector3.zero;
        rect.localRotation = Quaternion.identity;
        rect.localScale = new Vector3(1f, 1f, 1f);
        rect.sizeDelta = new Vector2(0, 0);
    }


    void GoToIndex(GameObject go, int Index)
    {
        if (Index < 0 || Index >= go.transform.parent.childCount) return;
        go.transform.SetSiblingIndex(Index);
    }

    bool CheckLayerName(string Layer)
    {
        bool value = true;

        if (!Layer.Contains("Layer"))
        {
            value = false;
        }

        int index = -1;
        value = int.TryParse(Layer.Substring(0, 5), out index);
        return value;
    }

    int GetNearestAboveLayerIndex(int Index)
    {
        int value = -1;
        for (int i = Index - 1; i >= 0; i--)
        {
            if (CanvasUIC.Instance.transform.Find("Layer" + i.ToString()) != null)
            {
                value = i;
                break;
            }
        }
        return value;
    }

    int GetLayerIndex(string Layer)
    {
        string l = Layer.Remove(0, 5);
        return int.Parse(l);
    }

    public void SetChildIndex(int Index)
    {
        this.ChildIndex = Index;
    }

    public void DoChildIndex()
    {
        if (ChildIndex == -1 || ChildIndex >= transform.parent.childCount) return;
        transform.SetSiblingIndex(this.ChildIndex);
    }
}

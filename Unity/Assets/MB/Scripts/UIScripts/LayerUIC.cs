﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerUIC : LayerUI
{
    public override void Awake()
    {
        base.Awake();
        StartCoroutine(C_Awake());
    }    

    IEnumerator C_Awake()
    {
        yield return new WaitUntil(() => CanvasUIC.Instance != null);
        CanvasTransform = CanvasUIC.Instance.transform;
    }
}

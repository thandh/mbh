﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerUIO : LayerUI
{
    public override void Awake()
    {
        base.Awake();
        StartCoroutine(C_Awake());
    }

    IEnumerator C_Awake()
    {
        yield return new WaitUntil(() => CanvasUIO.Instance != null);
        CanvasTransform = CanvasUIO.Instance.transform;
    }
}

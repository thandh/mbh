﻿using Beetsoft.UI.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beetsoft.UI.Scripts
{
    public enum UIState
    {
        Nothing,
        Hiden,
        Open,
        Close
    }

    public class UIBase : MonoBehaviour
    {
        public bool isEverythingAboveReady = false;
        public UIState uiState = UIState.Nothing;

        private Animator animator = null;
        public Animator Animator
        {
            get
            {
                if (animator == null) animator = GetComponent<Animator>();
                return animator;
            }
        }

        public virtual void Awake()
        {

        }

        public void ResetRect()
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = new Vector3(1f, 1f, 1f);
            GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
        }

        public virtual void SeftHide()
        {
            gameObject.SetActive(false);
            uiState = UIState.Hiden;
        }        

        public virtual void SeftDestroy()
        {
            Destroy(gameObject);
        }

        public static void CallDestroy(MonoBehaviour mono)
        {
            Destroy(mono.gameObject);
        }

        public virtual void Open()
        {
            if (uiState == UIState.Hiden) gameObject.SetActive(true);

            GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
            AnimatorOpen(Animator);
            uiState = UIState.Open;
        }

        public virtual void Close()
        {
            if (uiState == UIState.Hiden) gameObject.SetActive(true);

            AnimatorClose(Animator);
            uiState = UIState.Close;
        }
		
		public virtual void OpenInEditor()
        {
            if (uiState == UIState.Hiden) gameObject.SetActive(true);

            GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
            uiState = UIState.Open;
        }

        public virtual void CloseInEditor()
        {
            if (uiState == UIState.Hiden) gameObject.SetActive(true);

            GetComponent<RectTransform>().localScale = Vector3.zero;
            uiState = UIState.Close;
        }

        public void AnimatorOpen(Animator Animator)
        {
            if (Animator == null)
                return;

            try
            {
                Animator.SetBool("Idle", false);
                Animator.SetBool("Open", true);
                Animator.SetBool("Close", false);
            }
            catch { Debug.Log(Animator.name + " can't open"); }
        }

        public void AnimatorClose(Animator Animator)
        {
            if (Animator == null)
                return;

            try
            {
                Animator.SetBool("Idle", false);
                Animator.SetBool("Open", false);
                Animator.SetBool("Close", true);
            }
            catch { Debug.Log(Animator.name + " can't close"); }
        }

        public virtual void HideAllChildren(Transform tr)
        {
            foreach (Transform trChild in tr.GetComponentsInChildren<Transform>())
            {
                if (trChild != tr) trChild.gameObject.SetActive(false);
            }
        }
		
		public virtual void ShowOneHideAllSiblings(GameObject go)
        {
            Transform parent = go.transform.parent;

            for (int i = 0; i < parent.childCount; i++)
            {
                go.SetActive(parent.GetChild(i).gameObject == go);
            }
        }

        public virtual void DeleteAllChildren(Transform tr)
        {
            if (Application.isPlaying)
                StartCoroutine(C_DeleteAllChildren(tr));
            else E_DeleteAllChildren(tr);
        }

        IEnumerator C_DeleteAllChildren(Transform tr)
        {
            int childCount = tr.childCount;
            for (int i = 0; i < childCount; i++)
            {
                Destroy(tr.GetChild(tr.childCount - 1).gameObject);
                yield return new WaitUntil(() => tr.childCount == childCount - i - 1);
            }
        }

        void E_DeleteAllChildren(Transform tr)
        {
            int childCount = tr.childCount;
            for (int i = 0; i < childCount; i++)
            {
                DestroyImmediate(tr.GetChild(tr.childCount - 1).gameObject);
            }
        }

        public void SetLastChild()
        {
            if (IsLastChild()) return;

            int childCount = transform.parent.childCount;
            transform.SetSiblingIndex(childCount - 1);
        }

        public int GetChildIndex()
        {
            return (transform.GetSiblingIndex());
        }

        public bool IsLastChild()
        {
            return (transform.GetSiblingIndex() == transform.parent.childCount - 1);
        }        

        /// <summary>
        /// Method to test call LoadAScene with effect
        /// </summary>
        /// <param name="2 names"></param>
        public void LoadAScene(string nameCombine)
        {
            string[] names = nameCombine.Split(',');

            if (names.Length != 2)
            {
                Debug.LogWarning("Nhap ten chua dung!! Nhap 2 ten, cach nhau dau phay!");
                return;
            }

            UILoadScene.Instance.LoadAScene(names[0], names[1], true);
        }
    }
}
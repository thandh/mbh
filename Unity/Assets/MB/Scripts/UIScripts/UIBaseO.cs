﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Beetsoft.UI.Scripts
{
    [RequireComponent(typeof(LayerUIO))]
    public class UIBaseO<T> : UIBase where T : MonoBehaviour
    {
        private static object _lock = new object();

        private static T _instance;

        public static T Instance
        {
            get
            {
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        UnityEngine.Object[] Ts = FindObjectsOfType(typeof(T));

                        if (Ts.Length > 1)
                        {
                            _instance = (T)Ts[0];

                            for (int i = 1; i < Ts.Length; i++)
                            {
                                CallDestroy((T)Ts[i]);
                            }

                            return _instance;
                        }
                        else if (Ts.Length == 0)
                        {
                            GameObject singleton = (GameObject)Instantiate(Resources.Load("UIPrefabs/" + (typeof(T)).ToString()) as GameObject);

                            if (singleton == null)
                            {
                                Debug.LogError("Can't find prefabs " + "/UIPrefabs/" + (typeof(T)).ToString());
                            }
                            else
                            {
                                singleton.transform.localPosition = Vector3.zero;
                                singleton.transform.localRotation = Quaternion.identity;
                                singleton.transform.localScale = new Vector3(1f, 1f, 1f);
                                singleton.name = (typeof(T)).ToString();

                                singleton.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);

                                _instance = singleton.GetComponent<T>();
                            }
                        }
                    }

                    return _instance;
                }
            }
        }

        float readyTimeReady = 2f;
        private LayerUIO _layerUI = null;
        private LayerUIO layerUI { get { if (_layerUI == null) _layerUI = GetComponent<LayerUIO>(); return _layerUI; } }
        
        public override void Awake()
        {
            base.Awake();

            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            StartCoroutine(C_Awake());
        }

        IEnumerator C_Awake()
        {
            float time = Time.time;
            yield return new WaitUntil(() => CanvasUIO.Instance != null || Time.time - time > readyTimeReady);

            if (CanvasUIO.Instance == null && Time.time - time > readyTimeReady)
            {
                isEverythingAboveReady = false;
                Debug.LogWarning(typeof(T).ToString() + " not ready ");
                yield break;
            }
            else if (CanvasUIO.Instance != null)
            {
                isEverythingAboveReady = true;
            }

            if (isEverythingAboveReady)
            {
                _instance = GetComponent<T>();
                DoLayer();
                uiState = UIState.Open;
            }
        }        

        public void SetLayer(string Layer)
        {
            layerUI.SetLayer(Layer);
        }

        public void DoLayer()
        {
            layerUI.DoLayer();
        }
    }
}
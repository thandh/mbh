﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Beetsoft.Browser.Scripts;
using Beetsoft.UI.Scripts;

public class UICamCapture : UIBaseC<UICamCapture>
{
    private bool camAvailable = false;
    private WebCamTexture WebCamTexture;
    private Texture defaultBackground;

    public RawImage RawImage;
    public AspectRatioFitter fit;

    private BrowserCaller _browsercaller = null;
    private BrowserCaller browserCaller { get { if (_browsercaller == null) _browsercaller = FindObjectOfType<BrowserCaller>(); return _browsercaller; } }    

    private string RootDir { get { return Application.persistentDataPath + "/" + "Data/"; } }

    private string PNGSaveDir { get { return RootDir + "PNG/"; } }

    private void Start()
    {
        defaultBackground = RawImage.texture;
        WebCamDevice[] devices = WebCamTexture.devices;

        if (devices.Length == 0)
        {
            Debug.Log("No camera detected!!");
            camAvailable = false;
            return;
        }

        for (int i = 0; i < devices.Length; i++)
        {
            if (!devices[i].isFrontFacing)
            {
                WebCamTexture = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
            }
        }

        if (WebCamTexture == null)
        {
            Debug.Log("Unable to find back camera");
            return;
        }
    }

    private void Update()
    {
        if (!camAvailable) return;

        float ratio = (float)WebCamTexture.width / (float)WebCamTexture.height;
        fit.aspectRatio = ratio;

        float scaleY = WebCamTexture.videoVerticallyMirrored ? -1f : 1f;
        RawImage.rectTransform.localScale = new Vector3(1f, scaleY, 1f);

        int orient = -WebCamTexture.videoRotationAngle;
        RawImage.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
    }

    public override void Open()
    {
        base.Open();

        if (WebCamTexture != null)
        {
            WebCamTexture.Play();
            RawImage.texture = WebCamTexture;
        }
    }

    public override void Close()
    {
        base.Close();

        if (WebCamTexture != null)
        {
            WebCamTexture.Stop();
        }
    }

    public void Capture()
    {           
        StartCoroutine(C_Capture());        
    }

    IEnumerator C_Capture()
    {
        if (WebCamTexture != null)
        {
            WebCamTexture.Pause();
            yield return new WaitUntil(() => !WebCamTexture.isPlaying);

            Texture2D Texture2D = new Texture2D(Screen.width, Screen.height, TextureFormat.ARGB32, false);
            Texture2D.SetPixels32(WebCamTexture.GetPixels32());
            browserCaller.UpdatePNGtoSave(Texture2D);
        }

        browserCaller.UpdateExtension("png");
        browserCaller.OpenFileBrowser(true);

        yield return new WaitForSeconds(0.5f);
        WebCamTexture.Play();
    }    
}

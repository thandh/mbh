﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Beetsoft.UI.Scripts;
using SceneControlling;

public class UIFloor : UIBaseC<UIFloor>
{
    FloorSC _FloorSC = null;
    FloorSC FloorSC { get { if (_FloorSC == null) _FloorSC = FindObjectOfType<FloorSC>(); return _FloorSC; } }

    FloorDataController _FloorDataController = null;
    FloorDataController FloorDataController { get { if (_FloorDataController == null) _FloorDataController = FloorSC.FloorDataController; return _FloorDataController; } }

    public Transform floorButtonHolder;

    public void LoadSprites()
    {
        FloorDataController.LoadSprites();

        DeleteAllChildren(floorButtonHolder);

        for (int i = 0; i < FloorDataController.Sprites.Length; i++)
        {
            GameObject gobutton = (GameObject)Instantiate(Resources.Load("UIPrefabs/Floor/FloorButton") as GameObject);
            FloorButton FloorButton = gobutton.GetComponent<FloorButton>();

            FloorButton.LoadImage(FloorDataController.Sprites[i]);
            FloorButton.ResetTrans();
        }
    }    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SceneControlling;
using Beetsoft.UI.Scripts;

public class UIIntro : UIBaseC<UIIntro>
{
    IntroSC _Intro = null;
    IntroSC Intro { get { if (_Intro == null) _Intro = FindObjectOfType<IntroSC>(); return _Intro; } }

    public override void Awake()
    {
        base.Awake();
    }

    public void OnImageClick(string laterScene)
    {
        UILoadScene.Instance.LoadAScene(Intro.name, laterScene);
        Intro.Unload();
        SeftDestroy();
    }    
}

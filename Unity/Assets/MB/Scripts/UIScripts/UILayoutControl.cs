﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SceneControlling;
using Beetsoft.UI.Scripts;
using UnityEngine.UI;
using Beetsoft.Layout.CameraControl;
using System;
using Beetsoft.World.Layouts.Point;

public class UILayoutControl : UIBaseC<UILayoutControl>
{
    private LayoutSC _LayoutSC = null;
    private LayoutSC LayoutSC { get { if (_LayoutSC == null) _LayoutSC = FindObjectOfType<LayoutSC>(); return _LayoutSC; } }

    private Points _points = null;
    private Points Points { get { if (_points == null) _points = FindObjectOfType<Points>(); return _points; } }    

    public override void Awake()
    {
        base.Awake();
        StartCoroutine(C_Awake());
    }

    IEnumerator C_Awake()
    {
        yield return new WaitUntil(() => CameraWorld.Instance != null);
        CameraWorld.Instance.isAllow = true;
        Points.LockAll();
    }
    
    public void btnLayoutClick(Toggle Toggle)
    {
        
    }

    public void btnMoveClick(Toggle Toggle)
    {
        CameraWorld.Instance.isAllow = Toggle.isOn;
        if (Toggle.isOn) Points.LockAll(); else Points.UnLockAll();
    }
}

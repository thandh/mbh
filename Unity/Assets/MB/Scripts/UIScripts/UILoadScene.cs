﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using SceneControlling;
using Beetsoft.UI.Scripts;

public class UILoadScene : UIBaseO<UILoadScene>
{
    [SerializeField] private Image bg;
    [SerializeField] private float speed = .1f;
        
    [SerializeField] private float destination = 0f;

    private void Start()
    {
        
    }

    public void AlphaIn()
    {
        SetLastChild();

        destination = 1;        
    }

    public void AlphaOut()
    {
        SetLastChild();

        destination = 0;        
    }   

    public void SetAlpha(float alpha)
    {
        SetLastChild();

        destination = alpha;        
    }

    public void AlphaInEditor()
    {
        SetLastChild();

        destination = 1;
        bg.color = new Color(bg.color.r, bg.color.g, bg.color.b, destination);
    }

    public void AlphaOutEditor()
    {
        SetLastChild();

        destination = 0;
        bg.color = new Color(bg.color.r, bg.color.g, bg.color.b, destination);
    }

    private void Update()
    {
        if (bg.color.a == destination) return;

        //float direction = bg.color.a < destination ? 1f : -1f;
        bg.color = new Color(bg.color.r, bg.color.g, bg.color.b, Mathf.Lerp(bg.color.a, destination, speed));
        if (Mathf.Abs(bg.color.a - destination) < 0.05f)
        {
            bg.color = new Color(bg.color.r, bg.color.g, bg.color.b, destination);            
        }

        //Debug.Log(bg.color.a + " " + destination);
    }

    public bool IsAlphaOut()
    {
        return bg.color.a == 0;
    }

    public bool IsAlphaIn()
    {
        return bg.color.a == 1;
    }

    public bool IsAlpha(float alpha)
    {
        return bg.color.a == alpha;
    }

    public void LoadAScene(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void LoadAScene(string previousSC, string laterSC, bool forceLoad = false)
    {
        StartCoroutine(C_LoadAScene(previousSC, laterSC));
    }

    IEnumerator C_LoadAScene(string previousSC, string laterSC, bool forceLoad = false)
    {
        float time = Time.time;

        yield return new WaitUntil(() => GameObject.Find(previousSC) != null || Time.time - time > 2f);

        GameObject GOpreviousSC = GameObject.Find(previousSC);

        if (Time.time - time > 2 && GOpreviousSC == null)
        {
            Debug.LogWarning("Khong tim thay (GO) pre-SceneControl " + previousSC);
            yield break;
        }

        SceneControl PreviousSC = GOpreviousSC.GetComponent<SceneControl>();

        if (PreviousSC == null)
        {
            Debug.LogWarning("Khong tim thay (SC) pre-SceneControl " + previousSC);
            yield break;
        }

        if (forceLoad)
        {
            PreviousSC.Unload();
        }

        AlphaIn();

        yield return new WaitUntil(() => PreviousSC.SceneState == eSceneState.Unloaded && IsAlphaIn());

        SceneManager.LoadScene(laterSC);

        time = Time.time;

        yield return new WaitUntil(() => GameObject.Find(laterSC) != null || Time.time - time > 2f);

        GameObject GOlaterSC = GameObject.Find(laterSC);

        if (Time.time - time > 2 && GOlaterSC == null)
        {
            Debug.LogWarning("Khong tim thay (GO) late-SceneControl " + laterSC);
            yield break;
        }

        SceneControl LaterSC = GOlaterSC.GetComponent<SceneControl>();

        if (LaterSC == null)
        {
            Debug.LogWarning("Khong tim thay (SC) late-SceneControl " + laterSC);
            yield break;
        }

        if (forceLoad)
        {
            LaterSC.Load();
        }

        yield return new WaitUntil(() => LaterSC.SceneState == eSceneState.Loaded);

        AlphaOut();
    }

    public void LoadasycnAScene(string name)
    {
        SceneManager.LoadSceneAsync(name);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SceneControlling;
using Beetsoft.UI.Scripts;

public class UIMainMenu : UIBaseC<UIMainMenu>
{
    MenuSelectionSC _MenuSelectionSC = null;
    MenuSelectionSC MenuSelectionSC { get { if (_MenuSelectionSC == null) _MenuSelectionSC = FindObjectOfType<MenuSelectionSC>(); return _MenuSelectionSC; } }

    public override void Awake()
    {
        base.Awake();        
    }

    public void OnImageClick(string laterScene)
    {
        UILoadScene.Instance.LoadAScene(MenuSelectionSC.name, laterScene);
        MenuSelectionSC.Unload();
        SeftDestroy();
    }    
}

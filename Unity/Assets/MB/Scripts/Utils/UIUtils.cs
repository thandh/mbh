﻿using SceneControlling;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIUtils : MonoBehaviour
{
	public void LoadScene(string laterSceneSC)
    {
        SceneControl sceneControl = FindObjectOfType<SceneControl>();
        UILoadScene.Instance.LoadAScene(sceneControl.name, laterSceneSC);
        sceneControl.Unload();
    }
}

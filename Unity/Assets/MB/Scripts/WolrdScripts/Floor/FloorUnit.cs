﻿using Beetsoft.Layout.CameraControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Beetsoft.World.Floors
{
    public enum eFloorUnitState
    {
        Nothing,
        AboveUI,
        Selected
    }

    public class FloorUnit : MonoBehaviour
    {
        [SerializeField] Sprite _Sprite = null;

        [SerializeField] private bool _isLock = false;
        public bool IsLock { get { return _isLock; } set { _isLock = value; } }

        [SerializeField] private bool _isPlaced = false;
        public bool IsPlaced { get { return _isPlaced; } set { _isPlaced = value;} }

        [SerializeField] private bool _isPlacedPossible = true;        

        [SerializeField] private Color DefaultColor_Nothing = Color.white;
        [SerializeField] private Color DefaultColor_Highlighted = Color.red;
        [SerializeField] private Color DefaultColor_Selected = Color.red;

        private Floors _Floors = null;
        public Floors Floors { get { if (_Floors == null) _Floors = FindObjectOfType<Floors>(); return _Floors; } }

        SpriteRenderer _SpriteRenderer = null;
        SpriteRenderer SpriteRenderer { get { if (_SpriteRenderer == null) _SpriteRenderer = GetComponent<SpriteRenderer>(); return _SpriteRenderer; } }

        BoxCollider _BoxCollider = null;
        BoxCollider BoxCollider { get { if (_BoxCollider == null) _BoxCollider = GetComponent<BoxCollider>(); return _BoxCollider; } }

        Transform _Parent = null;
        Transform Parent { get { if (_Parent == null) _Parent = FindObjectOfType<Floors>().transform; return _Parent; } }

        CameraWorld _CameraWorld = null;
        CameraWorld CameraWorld { get { if (_CameraWorld == null) _CameraWorld = FindObjectOfType<CameraWorld>(); return _CameraWorld; } }

        [SerializeField] private eFloorUnitState _eFloorUnitState = eFloorUnitState.Nothing;
        public eFloorUnitState eFloorUnitState
        {
            get { return _eFloorUnitState; }
            set
            {
                _eFloorUnitState = value;
                switch (_eFloorUnitState)
                {
                    case eFloorUnitState.Nothing: 
                        if (transform.position.z != 0) transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                        if (SpriteRenderer.material.color != DefaultColor_Nothing) SpriteRenderer.material.color = DefaultColor_Nothing;
                        break;
                    case eFloorUnitState.AboveUI:
                        if (transform.position.z != 1f) transform.position = new Vector3(transform.position.x, transform.position.y, 1f);
                        break;
                    case eFloorUnitState.Selected:
                        if (transform.position.z != 0) transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                        if (SpriteRenderer.material.color != DefaultColor_Selected) SpriteRenderer.material.color = DefaultColor_Selected;
                        break;
                }
            }
        }

        private Vector3 offset = Vector3.zero;
        private Vector3 screenPoint = Vector3.zero;
        private float currentZToState
        {
            get
            {
                switch (eFloorUnitState)
                {
                    default: return 0;
                    case eFloorUnitState.Nothing: return 0;
                    case eFloorUnitState.AboveUI: return 1;
                    case eFloorUnitState.Selected: return 0;
                }
            }
        }

        public void SetIsPlace()
        {
            SetIsPlace(_isPlaced);
        }

        public void SetIsPlace(bool IsPlaced)
        {
            this.IsPlaced = IsPlaced;
        }

        public void LoadSprite()
        {
            if (_Sprite == null) return;
            LoadSprite(_Sprite);
        }

        public void LoadSprite(Sprite Sprite)
        {
            SetImage(Sprite);
            SpriteRenderer.sprite = Sprite;            
            if (Application.isPlaying) Destroy(BoxCollider); else DestroyImmediate(BoxCollider);
            gameObject.AddComponent<BoxCollider>();

            gameObject.name = Sprite.name;
        }

        public void SetImage(Sprite Sprite)
        {
            _Sprite = Sprite;
        }

        public void ResetTrans()
        {
            if (Parent == null) return;

            transform.SetParent(Parent);

            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = new Vector3(1, 1, 1);
        }

        public void InitPosition(Vector2 EventSystemPosition)
        {
            Vector3 wPosition = CameraWorld.GetComponent<Camera>().ScreenToWorldPoint(EventSystemPosition);
            transform.position = new Vector3(wPosition.x, wPosition.y, transform.position.z);
        }

        public void OnMouseDown()
        {
            if (IsLock) return;

            Vector3 scanPos = transform.position;
            screenPoint = CameraWorld.GetComponent<Camera>().WorldToScreenPoint(scanPos);

            offset = scanPos - CameraWorld.GetComponent<Camera>().ScreenToWorldPoint(
                new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        }

        void OnMouseDrag()
        {
            if (IsLock) return;

            if (IsPlaced && _eFloorUnitState != eFloorUnitState.Selected) return;

            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

            Vector3 curPosition = CameraWorld.GetComponent<Camera>().ScreenToWorldPoint(curScreenPoint) + offset;
            transform.position = new Vector3(curPosition.x, curPosition.y, curPosition.z);

            _isPlacedPossible = CheckisPlacedPossible();
        }

        bool CheckisPlacedPossible()
        {
            return true;
        }

        void OnMouseUp()
        {
            if (IsLock) return;

            if (eFloorUnitState == eFloorUnitState.AboveUI)
            {
                if (_isPlacedPossible)
                {
                    eFloorUnitState = eFloorUnitState.Nothing;
                    IsPlaced = true;
                }
                else
                {
                    IsPlaced = false;
                }

                if (!IsPlaced) SelfDestroy();
            }
            
            ///Debug.Log("On Mouse Up");            
        }

        void OnMouseUpAsButton()
        {
            if (IsLock) return;

            ///Debug.Log("On Mouse Up As Button");
            if (IsPlaced) SelectOnce();
        }

        public void SelectOnce()
        {
            if (eFloorUnitState == eFloorUnitState.Nothing)
                eFloorUnitState = eFloorUnitState.Selected;
            else if (eFloorUnitState == eFloorUnitState.Selected)
                eFloorUnitState = eFloorUnitState.Nothing;
        }

        public void UnSelect()
        {
            if (eFloorUnitState != eFloorUnitState.Selected) return;
            eFloorUnitState = eFloorUnitState.Nothing;
        }

        public bool IsSelected()
        {
            return eFloorUnitState == eFloorUnitState.Selected;
        }

        public void SelfDestroy()
        {   
            Floors.LoadFloorListDelay(gameObject);
            if (Application.isPlaying) Destroy(gameObject); else DestroyImmediate(gameObject);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beetsoft.World.Floors
{
    public class Floors : MonoBehaviour
    {
        public FloorUnit AFloorUnit = null;

        [SerializeField] List<FloorUnit> FloorUnitList = new List<FloorUnit>();

        public void LoadFloorUnitList()
        {
            FloorUnitList = new List<FloorUnit>();
            for (int i = 0; i < transform.childCount; i++)
            {
                FloorUnitList.Add(transform.GetChild(i).GetComponent<FloorUnit>());
            }
        }

        public void LoadFloorListDelay(GameObject o)
        {
            StartCoroutine(C_LoadFloorListDelay(o));
        }

        IEnumerator C_LoadFloorListDelay(GameObject o)
        {
            yield return new WaitUntil(() => o == null);
            LoadFloorUnitList();
        }

        public void CreateAFloorUnit(Vector2 position)
        {
            GameObject goAFloorUnit = (GameObject)Instantiate(Resources.Load("World/FloorUnit") as GameObject);
            AFloorUnit = goAFloorUnit.GetComponent<FloorUnit>();

            AFloorUnit.eFloorUnitState = eFloorUnitState.AboveUI;
            AFloorUnit.InitPosition(position);
            AFloorUnit.OnMouseDown();
        }
    }
}
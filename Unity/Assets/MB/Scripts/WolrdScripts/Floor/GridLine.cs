﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beetsoft.World.Grid
{
    public class GridLine : MonoBehaviour
    {
        [SerializeField] private int Index = -1;

        [SerializeField] private Color DefaultColor_Nothing = Color.white;
        [SerializeField] private Color DefaultColor_Highlighted = Color.cyan;

        private LineRenderer _lineRenderer = null;
        public LineRenderer lineRenderer { get { if (_lineRenderer == null) _lineRenderer = GetComponent<LineRenderer>(); return _lineRenderer; } }

        private Grids _GridsSC = null;
        public Grids GridsSC { get { if (_GridsSC == null) _GridsSC = FindObjectOfType<Grids>(); return _GridsSC; } }

        public void ResetTrans()
        {
            transform.SetParent(GridsSC.transform);

            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = new Vector3(1, 1, 1);

            Index = transform.GetSiblingIndex();
            gameObject.name = Index.ToString();            
        }

        public void SetPoints(Vector3 v1, Vector3 v2)
        {
            lineRenderer.SetPositions(new Vector3[2] { v1, v2 });
        }

        public void SelfDestroy()
        {
            if (Application.isPlaying) Destroy(gameObject); else DestroyImmediate(gameObject);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beetsoft.World.Grid
{
    public class Grids : MonoBehaviour
    {
        [SerializeField]
        Vector2 XBound = new Vector2(-100f, 100f);
        [SerializeField]
        Vector2 YBound = new Vector2(-100f, 100f);
        [SerializeField]
        float DefaultDistance = 0.91f;

        [SerializeField]
        List<GridLine> GridList = new List<GridLine>();

        [SerializeField]
        float _globalSize = 1f;

        public void LoadGridsList()
        {
            GridList = new List<GridLine>();
            for (int i = 0; i < transform.childCount; i++)
            {
                GridList.Add(transform.GetChild(i).GetComponent<GridLine>());
            }
        }

        public void RegenerateGridsEditor()
        {
            AnimationCurve curve = new AnimationCurve();
            curve.AddKey(0, _globalSize);
            curve.AddKey(1, _globalSize);

            float x = XBound.x;
            while (x < XBound.y)
            {
                GameObject goLine = (GameObject)Instantiate(Resources.Load("World/GridLine") as GameObject);
                GridLine gline = goLine.GetComponent<GridLine>();
                gline.ResetTrans();
                gline.SetPoints(new Vector3(x, YBound.x, transform.position.z), new Vector3(x, YBound.y, transform.position.z));
                gline.lineRenderer.widthCurve = curve;

                goLine = (GameObject)Instantiate(Resources.Load("World/GridLine") as GameObject);
                gline = goLine.GetComponent<GridLine>();
                gline.ResetTrans();
                gline.SetPoints(new Vector3(XBound.x, x, transform.position.z), new Vector3(XBound.y, x, transform.position.z));
                gline.lineRenderer.widthCurve = curve;

                x += DefaultDistance;
            }            
        }

        public void RegenerateGrids()
        {
            StartCoroutine(C_RegenerateGrids());
        }

        IEnumerator C_RegenerateGrids()
        {
            DeleteGrids();
            yield return new WaitUntil(() => transform.childCount == 0);

            float x = XBound.x;
            while (x < XBound.y)
            {
                GameObject goLine = (GameObject)Instantiate(Resources.Load("World/GridLine") as GameObject);
                GridLine gline = goLine.GetComponent<GridLine>();
                gline.ResetTrans();
                gline.SetPoints(new Vector3(x, YBound.x, transform.position.z), new Vector3(x, YBound.y, transform.position.z));

                goLine = (GameObject)Instantiate(Resources.Load("World/GridLine") as GameObject);
                gline = goLine.GetComponent<GridLine>();
                gline.ResetTrans();
                gline.SetPoints(new Vector3(XBound.x, x, transform.position.z), new Vector3(XBound.y, x, transform.position.z));

                x += DefaultDistance;
            }

            UpdateSizes();
        }

        public void DeleteGrids()
        {
            foreach (GridLine g in GridList)
            {
                g.SelfDestroy();
            }

            GridList = new List<GridLine>();
        }

        public void UpdateSizes()
        {
            AnimationCurve curve = new AnimationCurve();
            curve.AddKey(0, _globalSize);
            curve.AddKey(1, _globalSize);

            foreach (GridLine gl in GridList)
            {
                gl.lineRenderer.widthCurve = curve;
            }
        }
    }
}
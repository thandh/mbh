﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beetsoft.World.Layouts.Line
{
    using Layout.CameraControl;
    using Point;

    public enum eLineState
    {
        Nothing,
        Selected,
        IsIntersected
    }

    public class Line : MonoBehaviour
    {
        [SerializeField] private int Index = -1;

        [SerializeField] private bool _isLock = true;
        public bool IsLock { get { return _isLock; } set { _isLock = value; } }

        [SerializeField] private Color DefaultColor_Nothing = Color.white;
        [SerializeField] private Color DefaultColor_Highlighted = Color.cyan;
        [SerializeField] private Color DefaultColor_Selected = Color.red;
        [SerializeField] private Color DefaultColor_IsIntersected = Color.magenta;

        private LineRenderer _lineRenderer = null;
        private LineRenderer lineRenderer { get { if (_lineRenderer == null) _lineRenderer = GetComponent<LineRenderer>(); return _lineRenderer; } }

        private Points _PointsSC = null;
        public Points PointsSC { get { if (_PointsSC == null) _PointsSC = FindObjectOfType<Points>(); return _PointsSC; } }        

        private Lines _LinesSC = null;
        public Lines LinesSC { get { if (_LinesSC == null) _LinesSC = FindObjectOfType<Lines>(); return _LinesSC; } }

        public Point p1 = null;
        public Point p2 = null;

        private eLineState _lastlineState = eLineState.Nothing;
        [SerializeField] private eLineState _lineState = eLineState.Nothing;
        public eLineState eLineState
        {
            get { return _lineState; }
            set
            {
                _lineState = value;
                switch (_lineState)
                {
                    case eLineState.Nothing:
                        lineRenderer.material.color = DefaultColor_Nothing;
                        break;
                    case eLineState.Selected:
                        lineRenderer.material.color = DefaultColor_Selected;
                        break;
                    case eLineState.IsIntersected:
                        lineRenderer.material.color = DefaultColor_IsIntersected;
                        break;
                }
            }
        }

        [SerializeField] private bool isHolding = false;
        [SerializeField] private float delayHold = 0.5f;

        void Awake()
        {
            eLineState = eLineState.Nothing;
            ResetTrans();
        }

        void OnEnable()
        {
            eLineState = eLineState.Nothing;
            ResetTrans();
        }

        public void ResetTrans()
        {
            transform.SetParent(LinesSC.transform);

            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = new Vector3(1, 1, 1);

            Index = transform.GetSiblingIndex();
            gameObject.name = Index.ToString();

            if (CameraWorld.Instance != null) SetLock(CameraWorld.Instance.isAllow);
            else SetLock(true);
        }

        public void UpdateLine()
        {
            if (p1 == null || p2 == null) return;

            lineRenderer.SetPositions(new Vector3[2] { p1.transform.position, p2.transform.position });

            if (isIntersected())
            {
                if (!PointsSC.isLock)
                {
                    PointsSC.isLock = true;
                }

                if (eLineState != eLineState.IsIntersected)
                {
                    _lastlineState = eLineState;
                    eLineState = eLineState.IsIntersected;
                }
            } else
            {
                if (PointsSC.isLock)
                {
                    PointsSC.isLock = !LinesSC.IsAllLineNotIntersectedOrTouch();
                }

                if (eLineState == eLineState.IsIntersected)
                {
                    eLineState = _lastlineState;
                }
            }
        }

        public bool isIntersected()
        {
            foreach (Line l in LinesSC.GetLineList())
            {
                if (l != this 
                    && LinesSC.IsIntersectingIn2dSpace(this, l) 
                    && (!p1.IsPositionEqual(l.p1)) && (!p1.IsPositionEqual(l.p2))
                    && (!p2.IsPositionEqual(l.p1)) && (!p2.IsPositionEqual(l.p2)))
                {
                    return true;
                }
            }

            return false;
        }

        public bool isTouch(Line l)
        {
            return (p1.IsPositionEqual(l.p1) || p1.IsPositionEqual(l.p2) || p2.IsPositionEqual(l.p1) || p2.IsPositionEqual(l.p2));
        }

        public void SetPoints(Point p1, Point p2)
        {
            this.p1 = p1;
            this.p2 = p2;            
        }

        void OnMouseDown()
        {
            if (IsLock) return;

            ///Debug.Log("On Mouse Down");
            if (CameraWorld.Instance)

            StopCoroutine(C_CheckHold());
            StartCoroutine(C_CheckHold());
        }

        IEnumerator C_CheckHold()
        {
            if (IsLock) yield break;

            float time = Time.time;
            yield return new WaitUntil(() => Time.time - time > delayHold);
            isHolding = true;
            Select();
        }

        void OnMouseDrag()
        {
            if (IsLock) return;

            ///Debug.Log("On Mouse Drag");
            if (eLineState != eLineState.Selected) return;
        }

        void OnMouseUp()
        {
            if (IsLock) return;

            ///Debug.Log("On Mouse Up");
            isHolding = false;
        }

        void OnMouseUpAsButton()
        {
            if (IsLock) return;

            ///Debug.Log("On Mouse Up As Button");
            SelectOnce();
        }

        public void SetLock(bool Lock)
        {
            IsLock = Lock;
        }

        public void Select()
        {
            if (eLineState != eLineState.Nothing) return;
            eLineState = eLineState.Selected;
        }

        public void SelectOnce()
        {
            if (eLineState != eLineState.Nothing) return;
            eLineState = eLineState.Selected;
            LinesSC.UnSelectAllButThis(this);
        }

        public void UnSelect()
        {
            if (eLineState != eLineState.Selected) return;
            eLineState = eLineState.Nothing;
        }

        public bool IsSelected()
        {
            return eLineState == eLineState.Selected;
        }

        public void SelfDestroy()
        {
            int index = LinesSC.GetLineList().FindIndex((x) => x == this);
            if (index != -1) LinesSC.GetLineList().RemoveAt(index);

            CallPointReloadLines();

            if (Application.isPlaying) Destroy(gameObject); else DestroyImmediate(gameObject);
        }

        public void CallPointReloadLines()
        {
            PointsSC.LoadPointList();
            foreach(Point p in PointsSC.GetPointList())
            {
                p.ReloadLines();
            }           
        }

        public bool IsPolygonEdge()
        {
            bool isSet = false;
            bool compareLeft = false;

            foreach (Point p in PointsSC.GetPointList())
            {
                if (p != p1 && p != p2)
                {
                    if (!isSet) { compareLeft = LinesSC.isLeft(p1, p2, p); isSet = true; continue; }
                    if (LinesSC.isLeft(p1, p2, p) != compareLeft) return false;
                }
            }

            return true;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beetsoft.World.Layouts.Line
{
    using Point;

    public class Lines : MonoBehaviour
    {
        [SerializeField]
        List<Line> LineList = new List<Line>();

        private Points _PointsSC = null;
        public Points PointsSC { get { if (_PointsSC == null) _PointsSC = FindObjectOfType<Points>(); return _PointsSC; } }

        public void DeleteLines()
        {
            foreach (Line l in LineList)
            {
                l.SelfDestroy();
            }

            LineList = new List<Line>();
        }

        public void LoadLineList()
        {
            LineList = new List<Line>();
            for (int i = 0; i < transform.childCount; i++)
            {
                LineList.Add(transform.GetChild(i).GetComponent<Line>());
            }
        }

        public List<Line> GetLineList()
        {
            return LineList;
        }

        public void UnSelectAll()
        {
            foreach (Line p in LineList)
            {
                p.UnSelect();
            }
        }

        public void UnSelectAllButThis(Line point)
        {
            foreach (Line p in LineList)
            {
                if (p != point) p.UnSelect();
            }
        }

        public void UnSelectAllButSelectThis(Line point)
        {
            foreach (Line p in LineList)
            {
                if (p != point) p.UnSelect(); else p.Select();
            }
        }

        public void LockAll()
        {
            foreach (Line p in LineList)
            {
                p.SetLock(true);
            }
        }

        public void UnLockAll()
        {
            foreach (Line p in LineList)
            {
                p.SetLock(false);
            }
        }

        public Line GetLineWithTwoPoints(Point p1, Point p2)
        {            
            foreach (Line l in LineList)
            {
                if ((l.p1 == p1 && l.p2 == p2) || (l.p1 == p2 && l.p2 == p1))
                    return l;
            }
            return null;
        }

        void OnMouseDown()
        {
            ///Debug.Log("On Mouse Down");            
        }

        void OnMouseDrag()
        {
            ///Debug.Log("On Mouse Drag");            
        }

        void OnMouseUp()
        {
            ///Debug.Log("On Mouse Up");
        }

        void OnMouseUpAsButton()
        {
            ///Debug.Log("On Mouse Up As Button");
            UnSelectAll();
        }

        void Update()
        {
            
        }

        public bool IsAllLineNotIntersectedOrTouch()
        {
            for (int i = 0; i < LineList.Count - 1; i++)
                for (int j = i + 1; j < LineList.Count; j++)
                {
                    if (LineList[i].isTouch(LineList[j])) continue;

                    if (IsIntersectingIn2dSpace(LineList[i], LineList[j]))
                    {
                        return false;
                    }
                }

            return true;
        }

        public bool IsIntersectingIn2dSpace(Line Line1, Line Line2)
        {
            return IsIntersectingIn2dSpace(Line1.p1.GetVector2(), Line1.p2.GetVector2(), Line2.p1.GetVector2(), Line2.p2.GetVector2());
        }

        public bool IsIntersectingIn2dSpace(Vector2 L1pos1, Vector2 L1pos2, Vector2 L2pos1, Vector2 L2pos2)
        {
            bool isIntersecting = false;
                        
            Vector2 p1 = new Vector2(L1pos1.x, L1pos1.y);
            Vector2 p2 = new Vector2(L1pos2.x, L1pos2.y);

            Vector2 p3 = new Vector2(L2pos1.x, L2pos1.y);
            Vector2 p4 = new Vector2(L2pos2.x, L2pos2.y);

            float denominator = (p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y);
                        
            if (denominator != 0)
            {
                float u_a = ((p4.x - p3.x) * (p1.y - p3.y) - (p4.y - p3.y) * (p1.x - p3.x)) / denominator;
                float u_b = ((p2.x - p1.x) * (p1.y - p3.y) - (p2.y - p1.y) * (p1.x - p3.x)) / denominator;
                                
                if (u_a >= 0 && u_a <= 1 && u_b >= 0 && u_b <= 1)
                {
                    isIntersecting = true;
                }
            }

            return isIntersecting;
        }
        
        ///With solution        
        public bool IsIntersecting(Vector2 L1pos1, Vector2 L1pos2, Vector2 L2pos1, Vector2 L2pos2)
        {
            bool isIntersecting = false;
            
            Vector2 l1_start = new Vector2(L1pos1.x, L1pos1.y);
            Vector2 l1_end = new Vector2(L1pos2.x, L1pos2.y);

            Vector2 l2_start = new Vector2(L2pos1.x, L2pos1.y);
            Vector2 l2_end = new Vector2(L2pos2.x, L2pos2.y);
            
            Vector2 l1_dir = (l1_end - l1_start).normalized;
            Vector2 l2_dir = (l2_end - l2_start).normalized;
            
            Vector2 l1_normal = new Vector2(-l1_dir.y, l1_dir.x);
            Vector2 l2_normal = new Vector2(-l2_dir.y, l2_dir.x);
            
            float A = l1_normal.x;
            float B = l1_normal.y;

            float C = l2_normal.x;
            float D = l2_normal.y;
                        
            float k1 = (A * l1_start.x) + (B * l1_start.y);
            float k2 = (C * l2_start.x) + (D * l2_start.y);
                        
            if (IsParallel(l1_normal, l2_normal))
            {
                Debug.Log("No solutions!");
                return isIntersecting;
            }
                        
            if (IsOrthogonal(l1_start - l2_start, l1_normal))
            {
                Debug.Log("Same line!");                
                return isIntersecting;
            }
            
            float x_intersect = (D * k1 - B * k2) / (A * D - B * C);
            float y_intersect = (-C * k1 + A * k2) / (A * D - B * C);

            Vector2 intersectPoint = new Vector2(x_intersect, y_intersect);
                        
            if (IsBetween(l1_start, l1_end, intersectPoint) && IsBetween(l2_start, l2_end, intersectPoint))
            {
                Debug.Log("Get point!");
                isIntersecting = true;
            }

            return isIntersecting;
        }
                
        public bool IsParallel(Vector2 v1, Vector2 v2)
        {            
            if (Vector2.Angle(v1, v2) == 0f || Vector2.Angle(v1, v2) == 180f)
            {
                return true;
            }

            return false;
        }
                
        public bool IsOrthogonal(Vector2 v1, Vector2 v2)
        {            
            if (Mathf.Abs(Vector2.Dot(v1, v2)) < 0.000001f)
            {
                return true;
            }

            return false;
        }
                
        public bool IsBetween(Vector2 a, Vector2 b, Vector2 c)
        {
            bool isBetween = false;
                        
            Vector2 ab = b - a;            
            Vector2 ac = c - a;
            
            if (Vector2.Dot(ab, ac) > 0f && ab.sqrMagnitude >= ac.sqrMagnitude)
            {
                isBetween = true;
            }

            return isBetween;
        }

        public bool isLeft(Point a, Point b, Point c)
        {
            return isLeft(a.GetVector2(), b.GetVector2(), c.GetVector2());
        }

        public bool isLeft(Vector2 a, Vector2 b, Vector2 c)
        {
            return ((b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x)) > 0;
        }
    }
}
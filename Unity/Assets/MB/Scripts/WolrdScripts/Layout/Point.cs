﻿using Beetsoft.Layout.CameraControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beetsoft.World.Layouts.Point
{
    using Line;

    public enum ePointState
    {
        Nothing, 
        Selected
    }   

    public class Point : MonoBehaviour
    {
        public int Index = -1;

        [SerializeField] private bool _isLock = true;
        public bool IsLock { get { return _isLock; } set { _isLock = value; } }

        [SerializeField] private Color DefaultColor_Nothing = Color.white;
        [SerializeField] private Color DefaultColor_Highlighted = Color.red;
        [SerializeField] private Color DefaultColor_Selected = Color.red;        

        public SpriteRenderer SpriteRenderer;

        private Points _PointsSC = null;
        public Points PointsSC { get { if (_PointsSC == null) _PointsSC = FindObjectOfType<Points>(); return _PointsSC; } }

        private Lines _LinesSC = null;
        public Lines LinesSC { get { if (_LinesSC == null) _LinesSC = FindObjectOfType<Lines>(); return _LinesSC; } }

        private Vector3 offset = Vector3.zero;
        private Vector3 screenPoint = Vector3.zero;

        [SerializeField] private List<Line> _Lines = new List<Line>();
        public List<Line> Lines
        {
            get { return _Lines; }
            set { _Lines = value; }
        }

        [SerializeField] private ePointState _pointState = ePointState.Nothing;
        public ePointState pointState
        {
            get { return _pointState; }
            set
            {
                _pointState = value;
                switch (_pointState)
                {
                    case ePointState.Nothing:
                        SpriteRenderer.material.color = DefaultColor_Nothing;
                        break;
                    case ePointState.Selected:
                        SpriteRenderer.material.color = DefaultColor_Selected;
                        break;
                }
            }
        }

        [SerializeField] private bool isHolding = false;
        [SerializeField] private float delayHold = 0.5f;

        void Awake()
        {
            pointState = ePointState.Nothing;
            ResetTrans();
        }

        void OnEnable()
        {
            pointState = ePointState.Nothing;
            ResetTrans();
        }

        public void ResetTrans()
        {
            transform.SetParent(PointsSC.transform);

            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = new Vector3(1, 1, 1);

            Index = transform.GetSiblingIndex();
            gameObject.name = Index.ToString();

            if (CameraWorld.Instance != null) SetLock(CameraWorld.Instance.isAllow);
            else SetLock(true);            
        }                

        public void CreateLineToPoint(Point Point)
        {
            if (Point == this) return;

            Line linetoFind = Lines.Find((x) => (x.p1 == Point || x.p2 == Point));
            if (linetoFind != null) return;

            GameObject goLine = (GameObject)Instantiate(Resources.Load("World/Line") as GameObject);
            Line line = goLine.GetComponent<Line>();
            line.ResetTrans();
            line.SetPoints(Point, this);
            line.UpdateLine();

            Lines.Add(line);
            Point.Lines.Add(line);

            Lines[0].LinesSC.LoadLineList();
        }

        public Line CreateLineToPointAndReturn(Point Point)
        {
            if (Point == this) return null;

            Line linetoFind = Lines.Find((x) => (x.p1 == Point || x.p2 == Point));
            if (linetoFind != null) return null;

            GameObject goLine = (GameObject)Instantiate(Resources.Load("World/Line") as GameObject);
            Line line = goLine.GetComponent<Line>();
            line.ResetTrans();
            line.SetPoints(Point, this);
            line.UpdateLine();

            Lines.Add(line);
            Point.Lines.Add(line);

            Lines[0].LinesSC.LoadLineList();

            return line;
        }

        void OnMouseDown()
        {
            if (IsLock) return;
            
            ///Debug.Log("On Mouse Down");
            if (CameraWorld.Instance)

            StopCoroutine(C_CheckHold());
            StartCoroutine(C_CheckHold());

            Vector3 scanPos = transform.position;            
            screenPoint = Camera.main.WorldToScreenPoint(scanPos);

            offset = scanPos - Camera.main.ScreenToWorldPoint(
                new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        }

        IEnumerator C_CheckHold()
        {
            if (IsLock) yield break;

            float time = Time.time;
            yield return new WaitUntil(() => Time.time - time > delayHold);
            isHolding = true;
            Select();
        }

        void OnMouseDrag()
        {
            if (IsLock) return;

            ReloadLines();

            ///Debug.Log("On Mouse Drag");
            if (pointState != ePointState.Selected) return;

            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
            transform.position = curPosition;
                        
            foreach (Line l in Lines) l.UpdateLine();            
        }

        void OnMouseUp()
        {
            if (IsLock) return;

            ///Debug.Log("On Mouse Up");
            isHolding = false;
        }

        void OnMouseUpAsButton()
        {
            if (IsLock) return;

            ///Debug.Log("On Mouse Up As Button");
            SelectOnce();
        }        

        public void SetLock(bool Lock)
        {
            IsLock = Lock;
        }

        public void Select()
        {
            if (pointState != ePointState.Nothing) return;
            pointState = ePointState.Selected;
        }

        public void SelectOnce()
        {
            if (pointState != ePointState.Nothing) return;
            pointState = ePointState.Selected;
            PointsSC.UnSelectAllButThis(this);            
        }

        public void UnSelect()
        {
            if (pointState != ePointState.Selected) return;
            pointState = ePointState.Nothing;            
        }

        public bool IsSelected()
        {
            return pointState == ePointState.Selected;
        }

        public void SelfDestroy()
        {
            ReloadLines();
            foreach (Line l in Lines) l.SelfDestroy();
            PointsSC.LoadPointListDelay(gameObject);
            if (Application.isPlaying) Destroy(gameObject); else DestroyImmediate(gameObject);
        }

        public void ReloadLines()
        {
            for (int i = 0; i < Lines.Count; i++)
            {
                if (Lines[i] == null)
                {
                    Lines.RemoveAt(i);
                    i--;
                }
            }
        }

        public Vector2 GetVector2()
        {
            return new Vector2(transform.position.x, transform.position.y);
        }

        public bool IsPositionEqual(Point p)
        {
            return Vector3.SqrMagnitude(transform.position - p.transform.position) < 0.01f;
        }
    }
}
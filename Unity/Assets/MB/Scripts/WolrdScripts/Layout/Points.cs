﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beetsoft.World.Layouts.Point
{
    using Line;

    public class Points : MonoBehaviour
    {
        public bool isLock = false;

        [SerializeField]
        List<Point> PointList = new List<Point>();

        private Lines _LinesSC = null;
        public Lines LinesSC { get { if (_LinesSC == null) _LinesSC = FindObjectOfType<Lines>(); return _LinesSC; } }

        public void CreatePoint()
        {
            if (isLock) { Debug.LogWarning("Da bi lock!!"); return; }

            GameObject goPoint = (GameObject)Instantiate(Resources.Load("World/Point") as GameObject);
            Point Point = goPoint.GetComponent<Point>();            
            Point.ResetTrans();

            Point pointtoAttach = PointList.Find((x) => x.Index == Point.Index - 1);

            if (pointtoAttach != null)
                Point.CreateLineToPoint(pointtoAttach);

            LoadPointList();
        }

        public void DeletePoints()
        {
            foreach (Point p in PointList) 
            {
                p.SelfDestroy();
            }

            PointList = new List<Point>();            
        }

        public void LoadPointListDelay(GameObject o)
        {
            StartCoroutine(C_LoadPointListDelay(o));
        }

        IEnumerator C_LoadPointListDelay(GameObject o)
        {
            yield return new WaitUntil(() => o == null);
            LoadPointList();
        }

        public void LoadPointList()
        {
            PointList = new List<Point>();
            for (int i = 0; i < transform.childCount; i++)
            {
                PointList.Add(transform.GetChild(i).GetComponent<Point>());
            }
        }

        public List<Point> GetPointList()
        {
            return PointList;
        }

        public void UnSelectAll()
        {
            foreach (Point p in PointList)
            {
                p.UnSelect();
            }
        }

        public void UnSelectAllButThis(Point point)
        {
            foreach (Point p in PointList)
            {
                if (p != point) p.UnSelect(); 
            }
        }

        public void UnSelectAllButSelectThis(Point point)
        {
            foreach (Point p in PointList)
            {
                if (p != point) p.UnSelect(); else p.Select();
            }
        }

        public void LockAll()
        {
            foreach (Point p in PointList)
            {
                p.SetLock(true);
            }
        }

        public void UnLockAll()
        {
            foreach (Point p in PointList)
            {
                p.SetLock(false);
            }
        }

        public void ConnectLastAndStartPoint()
        {
            PointList[PointList.Count - 1].CreateLineToPoint(PointList[0]);
        }

        public void ForceConnect()
        {
            StartCoroutine(C_ForceConnect());
        }

        IEnumerator C_ForceConnect()
        { 
            if (!LinesSC.IsAllLineNotIntersectedOrTouch()) yield break;

            int donecheck = -1;
            while (donecheck != 0)
            {
                donecheck = 0;
                for (int i = 0; i < PointList.Count - 1; i++)
                    for (int j = i + 1; j < PointList.Count; j++)
                        if (LinesSC.GetLineWithTwoPoints(PointList[i], PointList[j]) == null)
                        {
                            Line line = PointList[i].CreateLineToPointAndReturn(PointList[j]);
                            donecheck++;
                            if (line.isIntersected())
                            {
                                line.SelfDestroy();
                                yield return new WaitUntil(() => line == null);
                                donecheck--;
                            }
                        }
            }
        }

        public void ForceConnectInterior()
        {
            StartCoroutine(C_ForceConnectInterior());
        }

        IEnumerator C_ForceConnectInterior()
        {
            if (!LinesSC.IsAllLineNotIntersectedOrTouch()) yield break;

            int donecheck = -1;
            while (donecheck != 0)
            {
                donecheck = 0;
                for (int i = 0; i < PointList.Count - 1; i++)
                    for (int j = i + 1; j < PointList.Count; j++)
                        if (LinesSC.GetLineWithTwoPoints(PointList[i], PointList[j]) == null)
                        {
                            Line line = PointList[i].CreateLineToPointAndReturn(PointList[j]);
                            donecheck++;

                            if (line.IsPolygonEdge() || line.isIntersected())
                            {
                                line.SelfDestroy();
                                yield return new WaitUntil(() => line == null);
                                donecheck--;
                            }
                        }
            }
        }

        void OnMouseDown()
        {
            ///Debug.Log("On Mouse Down");            
        }

        void OnMouseDrag()
        {
            ///Debug.Log("On Mouse Drag");            
        }

        void OnMouseUp()
        {
            ///Debug.Log("On Mouse Up");
        }

        void OnMouseUpAsButton()
        {
            ///Debug.Log("On Mouse Up As Button");
            UnSelectAll();
        }
    }
}
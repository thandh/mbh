﻿using Beetsoft.World.Layouts.Point;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beetsoft.World
{   
    public class SpriteMB : MonoBehaviour
    {
        private SpriteRenderer _SpriteRenderer = null;
        public SpriteRenderer SpriteRenderer { get { if (_SpriteRenderer == null) _SpriteRenderer = GetComponent<SpriteRenderer>(); return _SpriteRenderer; } }

        private BoxCollider _BoxCollider = null;
        public BoxCollider BoxCollider { get { if (_BoxCollider == null) _BoxCollider = GetComponent<BoxCollider>(); return _BoxCollider; } }

        private Points _Points = null;
        public Points Points { get { if (_Points == null) _Points = FindObjectOfType<Points>(); return _Points; } }

        void OnMouseDown()
        {
            ///Debug.Log("On Mouse Down");            
        }

        void OnMouseDrag()
        {
            ///Debug.Log("On Mouse Drag");            
        }

        void OnMouseUp()
        {
            ///Debug.Log("On Mouse Up");
        }

        void OnMouseUpAsButton()
        {
            ///Debug.Log("On Mouse Up As Button");
            Points.UnSelectAll();
        }

        public void LoadASprite(Sprite Sprite = null)
        {
            SpriteRenderer.sprite = Sprite;
            BoxCollider.size = new Vector3(SpriteRenderer.sprite.bounds.size.x, SpriteRenderer.sprite.bounds.size.y, 0.1f);            
        }
    }
}
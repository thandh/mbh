﻿using Beetsoft.Layout.CameraControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beetsoft.World.Layouts
{
    public enum eWorldLayout
    {
        Nothing,
        CreatePoint,
        Move
    }

    public class WorldLayout : World
    {
        private CameraWorld _cameraWorld = null;
        private CameraWorld cameraWorld { get { if (_cameraGrid == null) _cameraWorld = FindObjectOfType<CameraWorld>(); return _cameraWorld; } }

        private CameraGrid _cameraGrid = null;
        private CameraGrid cameraGrid { get { if (_cameraGrid == null) _cameraGrid = FindObjectOfType<CameraGrid>(); return _cameraGrid; } }

        private CameraUI _cameraUI = null;
        private CameraUI cameraUI { get { if (_cameraUI == null) _cameraUI = FindObjectOfType<CameraUI>(); return _cameraUI; } }

        private SpriteMB _SpriteMB = null;
        public SpriteMB SpriteMB { get { if (_SpriteMB == null) _SpriteMB = GetComponentInChildren<SpriteMB>(); return _SpriteMB; } }        

        public void LoadASprite(Sprite Sprite = null)
        {
            SpriteMB.LoadASprite(Sprite);            
        }

        void Update()
        {
            if (!Input.GetMouseButton(0) && Input.touchCount == 0) return;

            Vector2 screenPoint = Input.GetMouseButton(0) ? (Vector2) Input.mousePosition : Input.GetTouch(0).position;

            RaycastHit hit; Ray ray = Camera.main.ScreenPointToRay((Vector3)screenPoint);

            if (Physics.Raycast(ray, out hit))
            {                
                ///Debug.Log(hit.transform.name + " " + hit.point);
            }
        }
    }
}